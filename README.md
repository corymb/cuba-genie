# CubaGenie

CubaGenie is a generator gem that sets up all the infrastructure you need in order to be immediately productive with the [Cuba](http://cuba.is/) web application. CubaGenie creates a skeleton Cuba app, configured to your preferences.

## Why use it?

[Cuba](http://cuba.is/) is a wonderful DSL that allows us to create simple web-apps quickly and easily. However, for more complex apps we would need to add extra functionality and components to the basic Cuba setup, things like view templates, data stores, static file serving, our favourite JS or CSS framework and so on. This can be a time-consuming task and it's not always obvious how to get everything working together. CubaGenie takes away the hassle by creating everything you need in one fell, quick swoop. 

## What does it create?
1. A basic Cuba application ready to be ran (config, app-file and Gemfile)
2. Ruby version management support files (.ruby-version and .ruby-gemset)
3. Standard view templates (layout and home)
4. A Minitest setup and basic functional test template (optional)
5. A Capybara setup and basic acceptance test template (optional)
6. A Rakefile with basic test tasks (optional)
6. A Twitter Bootstrap installation, hooked into the view templates (optional)


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cuba_genie'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cuba_genie

## Usage

In your shell, cd into your work directory and type:

```shell
cuba_genie new <your_app_name>
```

Then simply keep answering the questions CubaGenie asks you.

## Development

After checking out the repo, run `rake test` to run the tests. You can also run `rake console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Roadmap

* Add RSpec support
* Add choice of templating engine, e.g. erb, haml, slim, etc.
* Add Redis support
* Add relational DB support (possibly through an ORM)
* Create generator commands for test, model and view templates
* Add support for user authentication 


## Contributing

1. Fork the repository
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a merge request (https://gitlab.com/<user-name>/cuba-genie/merge_requests)


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

