$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
ENV['RACK_ENV'] = 'test'
require 'cuba_genie'

require 'minitest/autorun'
require 'minitest/reporters'
Minitest::Reporters.use! [ Minitest::Reporters::SpecReporter.new ]
