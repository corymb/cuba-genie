module CubaGenie

  class Command

    attr_reader :description, :error, :project_name

    def initialize(**args)
      @files_created, @dirs_created = [], []
    end

    def execute
      yield
      true
    rescue Exception => e
      puts e.message and false
    end

    def unexecute(message)
      puts message unless ENV['RACK_ENV'] == 'test'
      @files_created.each do |file_path|
        File.delete file_path if File.exist? file_path
      end
      @dirs_created.each do |dir_name|
        FileUtils.rm_rf(dir_name) if Dir.exist? dir_name
      end
    end

  end #class
end #CubaGenie
