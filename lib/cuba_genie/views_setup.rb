require 'fileutils'
require_relative 'content'
require_relative 'home_view_content'
require_relative 'layout_view_content'
require_relative 'command'


module CubaGenie

  ALLOWED_BOOTSTRAP_VERSIONS = %w(3.3.0 3.3.5 3.3.6 4.0.0-alpha.2)

  class ViewsSetup < Command
    require 'open-uri'
    require 'zip'

    include Content

    def initialize(**args)
      @project_name = args[:project_name]
      #TODO: allow for choice of different template engines
      @description = 'Setting up views'
      @bootstrap_version = args[:bootstrap_version] || 'n'
      super
    end

    def execute
      super do
        layout_view_formatters = [nil, @project_name.capitalize, nil]

        if ALLOWED_BOOTSTRAP_VERSIONS.include? @bootstrap_version
          bootstrap_location =
            "https://github.com/twbs/bootstrap/releases/download/v#{@bootstrap_version}/bootstrap-#{@bootstrap_version}-dist.zip"
            download_bootstrap_from bootstrap_location
          extract_bootstrap_to 'public'
          layout_view_formatters = [BOOTSTRAP_CSS, @project_name.capitalize, BOOTSTRAP_JS]
        end
        create_view_files(layout_view_formatters)
      end
    end

    def unexecute
      super("rolling back views")
    end


    private
    def create_view_files(layout_view_formatters)
      FileUtils.mkdir "views"
      @dirs_created << "views"
      home_file, layout_file = 'views/home.mote', 'views/layout.mote'
      File.open(home_file, 'w') {|f| f.write(HOME_VIEW_CONTENT % @project_name) }
      File.open(layout_file, 'w') {|f| f.write(LAYOUT_VIEW_CONTENT % layout_view_formatters) }
      @files_created << "#{Dir.pwd}/#{home_file}" << "#{Dir.pwd}/#{layout_file}"
    end



    def download_bootstrap_from(location)
      FileUtils.mkdir "temp"
      target = 'temp/bootstrap.zip'
      open(target, 'wb') do |file|
        file << open(location).read
      end

      # add the jumbotron css to zip file
      Zip::File.open(target, Zip::File::CREATE) do |zipfile|
        zipfile.get_output_stream("css/jumbotron.css") do |f|
          f.puts %Q(
            body {
              padding-top: 50px;
              padding-bottom: 20px;
            }
          )
        end
      end

      target
    end


    def extract_bootstrap_to(directory)
      FileUtils.mkdir directory

      Zip::File.open('temp/bootstrap.zip') do |zip_file|
        zip_file.each do |entry|
          puts "...extracting #{entry.name}"
          new_name = entry.name.gsub("bootstrap-#{@bootstrap_version}-dist", "")
          entry.extract("#{directory}/#{new_name}")

        end
      end

      @dirs_created << directory
      FileUtils.rm_rf('temp')
    end

  end #class
end #module
