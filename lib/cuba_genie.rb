require "cuba_genie/version"
require "cuba_genie/content"
require "cuba_genie/content_helpers"

require 'thor'
require_relative 'cuba_genie/command_list'
require_relative 'cuba_genie/cuba_setup'
require_relative 'cuba_genie/minitest_setup'
require_relative 'cuba_genie/views_setup'

module CubaGenie

  POSITIVE_RESPONSES =  %w(y Y Yes yes t true)
  NEGATIVE_RESPONSES =  %w(n N No no f false)


  class MyCLI < Thor
    include Content

    desc "new PROJECT", "create project NAME"
    def new(project_name)
      @cmds = CommandList.new
      puts "your project name is '#{project_name}'"
      minitest = want_minitest?
      reporter = (minitest ? choose_reporter : 'n')
      capybara = (minitest ? want_capybara? : false)
      @cmds.add_command CubaSetup.new(project_name: project_name, minitest: minitest, capybara: capybara)
      @cmds.add_command ViewsSetup.new(project_name: project_name, bootstrap_version: choose_bootstrap_version)
      @cmds.add_command MinitestSetup.new(project_name: project_name, reporter: reporter, capybara_setup: capybara) if minitest
      puts closing_message(project_name) if @cmds.execute
    end

    private
    def want_minitest?
      puts "Would you like to setup Minitest for your project?"
      ask_for_yes_no_response
    end

    def want_capybara?
      puts "Would you like to create acceptance (Capybara) tests for your project?"
      ask_for_yes_no_response
    end

    def choose_reporter
      puts "Choose a Minitest output format (reporter):"
      MINITEST_REPORTERS.each_with_index do |reporter, idx|
        puts "#{idx+1}: #{reporter.keys.first.to_s.extend(ContentHelpers).titlecase} (#{reporter.values.first})"
      end
      response = ask_for_numeric_reponse (1..8)
      puts ""

      MINITEST_REPORTERS[response.to_i - 1].keys.first.to_s.extend(ContentHelpers).titlecase
    end

    def choose_bootstrap_version
      puts "Choose which version of Twitter Bootstrap you want to use:"
      (["Don't install Bootstrap"] + ALLOWED_BOOTSTRAP_VERSIONS).each_with_index do |opt, idx|
        puts "#{idx+1}: #{opt}"
      end
      response = ask_for_numeric_reponse (1..5)
      puts ""

      (response.to_i == 1) ? 'n' : ALLOWED_BOOTSTRAP_VERSIONS[response.to_i - 2]
    end

    private

    def ask_for_yes_no_response
      response = STDIN.gets.chomp
      while !(POSITIVE_RESPONSES + NEGATIVE_RESPONSES).include? response
        puts "Enter y or n"
        response = STDIN.gets.chomp
      end
      puts ""
      POSITIVE_RESPONSES.include? response
    end


    # Prompts the user for a numeric input, within a valid range
    #
    # @param valid_range [String] captured STDIN
    #
    # @return [Fixnum] an integer within the valid range
    #
    # @note character 0 cannot be used as a range value. The reason is that 
    # #to_i will convert any non-numberic character to 0, which the method will
    # then wrongly interpret as a valid character
    #    
    def ask_for_numeric_reponse(valid_range)
      response = STDIN.gets.chomp.to_i
      while !valid_range.cover? response
        puts "Enter a number between #{valid_range.first}-#{valid_range.last}"
        response = STDIN.gets.chomp.to_i
      end
      puts ""
      response
    end

    def closing_message(project_name)
      "\nYour #{project_name} Cuba app is now set up and ready to go! To start the app, cd into #{project_name} directory, run 'bundle install' and then 'rackup'. You can then see your app running on 'http://localhost:9292'."
    end


  end #class
end #module
